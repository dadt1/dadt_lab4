package socket;

import org.json.JSONObject;

import java.io.*;
import java.net.Socket;

public class ServerController extends Thread {
    private final BufferedReader in; // поток чтения из сокета
    private final BufferedWriter out; // поток записи в сокет
    private boolean keepAlive;
    private final ServerService service = new ServerService();

    public ServerController(Socket socket) throws IOException {
        // сокет, через который сервер общается с клиентом, кроме него - клиент и сервер никак не связаны
        // если потоку ввода/вывода приведут к генерированию исключения, оно проброситься дальше
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        keepAlive = true;
        run(); // вызываем run()
    }

    @Override
    public void run() {
        try {
            while (keepAlive) {
                String request = in.readLine();
                // если пришедший запрос от пользователя не пустой,
                // тогда обрабатываем
                // иначе освобождаем клиента от сервера
                if (request != null) {
                    System.out.println("Пришедший запрос от пользователя: " + request);
                    JSONObject jsonObject = new JSONObject(request);
                    String requestType = jsonObject.toMap().get("requestType").toString();
                    switch (requestType) {
                        // переподключение пользователя
                        case "try_connect":
                            ServerController.this.send(service.tryUserConnection(ServerController.this, jsonObject));
                            break;
                        // зарегистрировать имя пользователя
                        case "take_username":
                            ServerController.this.send(service.takeUsername(jsonObject));
                            break;
                        // создание комнаты для игры
                        case "create_room":
                            ServerController.this.send(service.createRoom(ServerController.this, jsonObject));
                            break;
                        // подключение к комнате
                        case "connect_to_the_room":
                            ServerController.this.send(service.connectToRoom(ServerController.this, jsonObject));
                            break;
                        // занять ячейку
                        case "take_the_cell":
                            String lastMessage = service.takeCell(jsonObject);
                            if (lastMessage != null)
                                ServerController.this.send(lastMessage);
                            break;
                        // отключиться от комнаты
                        case "disconnect_from_the_room":
                            ServerController.this.send(service.disconnectFromRoom(jsonObject));
                            break;
                        // отключить пользователя от сервера
                        case "stop":
                            keepAlive = service.closeClientConnection(ServerController.this);
                            System.out.printf("Клиент %s отключился от сервера!%n", ServerController.this.getId());
                            break;
                        default:
                            ServerController.this.send("Запрос некорректен!");
                            break;
                    }
                } else {
                    keepAlive = service.closeClientConnection(ServerController.this);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    // отправить сообщение обратно клиенту
    public void send(String message) {
        try {
            out.write(String.format("%s%s", message,"\n"));
            out.flush();
        } catch (IOException e) {
            System.out.println(3);
            System.out.println(e.getMessage());
        }
    }
}