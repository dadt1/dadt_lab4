package socket;

import configs.Configs;
import dto.*;
import kafka.producer.Producer;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class ServerService {
    public static ArrayList<RoomDTO> roomList = new ArrayList<>();
    public static Integer currentRooms = 1;
    public static ArrayList<String> userNameList = new ArrayList<>();

    public String takeUsername (JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем имя пользователя
        // добавляем это имя в список пользователей
        String username = requestDTO.getRequestBody().getString("userName");
        UserNameResponseDTO userNameResponseDTO = new UserNameResponseDTO();
        if (!userNameList.contains(username)) {
            userNameList.add(username);
            userNameResponseDTO.setUserNameStatus("ok");
            // отправка в Kafka
            Producer.run(dataForKafka(requestDTO.getRequestType(),username, null, null, null));
            // отправка в Kafka
        } else {
            userNameResponseDTO.setUserNameStatus("failed");
        }
        // формируем ответ от сервера
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(requestDTO.getRequestType());
        responseDTO.setResponseBody(new JSONObject(userNameResponseDTO));
        // выводим список пользователей
        for (String currentUsername : userNameList) {
            System.out.println(currentUsername);
        }
        return new JSONObject(responseDTO).toString();
    }

    public String createRoom (ServerController serverController, JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем текущий номер комнаты
        Integer roomNumber;
        roomNumber = currentRooms;
        currentRooms++;
        // получаем имя пользователя
        // создаем комнату
        // добавляем комнату с писок
        String username = requestDTO.getRequestBody().getString("userName");
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setRoomId(roomNumber);
        roomDTO.setUserName1(username);
        roomDTO.setServerController1(serverController);
        roomDTO.setUserName2(null);
        roomDTO.setTicTacToeArray(new Integer[3][3]);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                roomDTO.getTicTacToeArray()[i][j] = 0;
                System.out.println(roomDTO.getTicTacToeArray()[i][j]);
            }
        }
        roomList.add(roomDTO);
        // отправка в Kafka
        Producer.run(dataForKafka(requestDTO.getRequestType(),username, roomNumber, null, null));
        // отправка в Kafka

        // формируем ответ от сервера
        RoomResponseDTO roomResponseDTO = new RoomResponseDTO();
        roomResponseDTO.setRoomStatus("ok");
        roomResponseDTO.setIdRoom(roomDTO.getRoomId());

        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(requestDTO.getRequestType());
        responseDTO.setResponseBody(new JSONObject(roomResponseDTO));

        return new JSONObject(responseDTO).toString();
    }
    public String connectToRoom (ServerController serverController, JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем имя пользователя и номер комнаты
        // если нашли номер комнаты
        // то добавляем найденной комнате пользователя
        // иначе возвращаем, что комната не найдена
        String userName = requestDTO.getRequestBody().getString("userName");
        Integer roomId = requestDTO.getRequestBody().getInt("idRoom");
        RoomResponseDTO roomResponseDTO = new RoomResponseDTO();
        roomResponseDTO.setRoomStatus("failed");
        boolean foundedRoom = false;
        for (RoomDTO room : roomList) {
            if (Objects.equals(room.getRoomId(), roomId)) {
                foundedRoom = true;
                room.setUserName2(userName);
                room.setServerController2(serverController);
                roomResponseDTO.setRoomStatus("ok");
                roomResponseDTO.setIdRoom(roomId);
                // отправка в Kafka
                Producer.run(dataForKafka(requestDTO.getRequestType(),userName, roomId, null, null));
                // отправка в Kafka
            }
        }
        // формируем ответ от сервера
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(requestDTO.getRequestType());
        if (foundedRoom) {
            responseDTO.setResponseBody(new JSONObject(roomResponseDTO));
        } else {
            responseDTO.setResponseBody(new JSONObject(roomResponseDTO));
        }
        System.out.println(new JSONObject(responseDTO).toString());
        return new JSONObject(responseDTO).toString();
    }
    public String takeCell (JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем имя пользователя, номер комнаты, номер клетки
        Integer idRoom = requestDTO.getRequestBody().getInt("idRoom");
        String username = requestDTO.getRequestBody().getString("userName");
        Integer cellNumber = requestDTO.getRequestBody().getInt("cellNumber");
        String newUsername = "";
        byte typeCell = 0;
        int j = cellNumber % 3;
        int i = cellNumber / 3;
        byte result = -1;
        boolean endGame = false;
        ServerController serverController1 = null;
        ServerController serverController2 = null;
        // находим комнату и добавляем крестик или нолик от соответствующего пользователя
        for (RoomDTO roomDTO : roomList) {
            if (Objects.equals(roomDTO.getRoomId(), idRoom)){
                if (Objects.equals(roomDTO.getUserName1(), username)) {
                    serverController2 = roomDTO.getServerController2();
                    roomDTO.getTicTacToeArray()[i][j] = 1;
                    newUsername = roomDTO.getUserName2();
                    typeCell = 1;
                } else {
                    if (Objects.equals(roomDTO.getUserName2(), username)) {
                        serverController1 = roomDTO.getServerController1();
                        roomDTO.getTicTacToeArray()[i][j] = 2;
                        newUsername = roomDTO.getUserName1();
                        typeCell = 2;
                    }
                }
                // выводим текущее состояние поля
                for (int q = 0; q < 3; q++) {
                    for (int w = 0; w < 3; w++) {
                        System.out.print(roomDTO.getTicTacToeArray()[q][w] + " ");
                    }
                    System.out.println();
                }
                result = whoWinGame(roomDTO.getTicTacToeArray());
                if (checkEndGame(roomDTO.getTicTacToeArray())) {
                    endGame = true;
                }
            }
        }
        ResponseDTO responseDTO = new ResponseDTO();
        if (!endGame) {
            responseDTO.setResponseType(requestDTO.getRequestType());
            CellResponseDTO cellResponseDTO = new CellResponseDTO();
            cellResponseDTO.setCellStatus("ok");
            cellResponseDTO.setUserName(newUsername);
            cellResponseDTO.setIdRoom(idRoom);
            cellResponseDTO.setCellNumber(cellNumber);
            cellResponseDTO.setTypeCell(typeCell);
            cellResponseDTO.setGameStatus("continue");
            // отправка в Kafka
            Producer.run(dataForKafka(requestDTO.getRequestType(), username, idRoom, cellNumber, (int) typeCell));
            // отправка в Kafka
            switch (result){
                case 0:
                    responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                    if (serverController1 != null) {
                        System.out.println("1 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());
                    }
                    if (serverController2 != null) {
                        System.out.println("2 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());
                    }
                    break;
                case 1:
                    if (serverController1 != null) {
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("3 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("4 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    if (serverController2 != null) {
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("5 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("6 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    break;
                case 2:
                    if (serverController1 != null) {
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("7 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("8 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    if (serverController2 != null) {
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("9 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("10 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    break;
            }
        } else {
            responseDTO.setResponseType(requestDTO.getRequestType());
            CellResponseDTO cellResponseDTO = new CellResponseDTO();
            cellResponseDTO.setCellStatus("ok");
            cellResponseDTO.setUserName(newUsername);
            cellResponseDTO.setIdRoom(idRoom);
            cellResponseDTO.setCellNumber(cellNumber);
            cellResponseDTO.setTypeCell(typeCell);
            cellResponseDTO.setGameStatus("draw");
            // отправка в Kafka
            Producer.run(dataForKafka(requestDTO.getRequestType(), username, idRoom, cellNumber, (int) typeCell));
            // отправка в Kafka
            switch (result){
                case 0:
                    responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                    if (serverController1 != null) {
                        System.out.println("11 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("12 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    if (serverController2 != null) {
                        System.out.println("13 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("14 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    break;
                case 1:
                    if (serverController1 != null) {
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("15 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("16 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    if (serverController2 != null) {
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("17 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("18 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    break;
                case 2:
                    if (serverController1 != null) {
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("19 " + new JSONObject(responseDTO));
                        serverController1.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("20 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    if (serverController2 != null) {
                        cellResponseDTO.setGameStatus("win");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("21 " + new JSONObject(responseDTO));
                        serverController2.send(new JSONObject(responseDTO).toString());

                        cellResponseDTO.setUserName(username);
                        cellResponseDTO.setGameStatus("lose");
                        responseDTO.setResponseBody(new JSONObject(cellResponseDTO));
                        System.out.println("22 " + new JSONObject(responseDTO));
                        return new JSONObject(responseDTO).toString();
                    }
                    break;
            }
        }
        return null;
    }
    public String disconnectFromRoom (JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем имя пользователя и номер комнаты
        String username = requestDTO.getRequestBody().getString("userName");
        Integer roomId = requestDTO.getRequestBody().getInt("idRoom");
        DisconnectRoomDTO disconnectRoom = new DisconnectRoomDTO();
        disconnectRoom.setUserName(username);
        disconnectRoom.setIdRoom(roomId);
        disconnectRoom.setDisconnectStatus("failed");
        // если нашли комнату, то убираем из нее пользователя
        // иначе возвращаем ответ пользователю, что от комнаты не смог отключиться
        for (RoomDTO room : roomList) {
            if (Objects.equals(room.getRoomId(), roomId)) {
                if (Objects.equals(room.getUserName1(), username)) {
                    room.setUserName1(null);
                    disconnectRoom.setDisconnectStatus("ok");
                    // отправка в Kafka
                    Producer.run(dataForKafka(requestDTO.getRequestType(), username, roomId, null, null));
                    // отправка в Kafka
                }
                if (Objects.equals(room.getUserName2(), username)) {
                    room.setUserName2(null);
                    disconnectRoom.setDisconnectStatus("ok");
                    // отправка в Kafka
                    Producer.run(dataForKafka(requestDTO.getRequestType(), username, roomId, null, null));
                    // отправка в Kafka
                }
            }
        }
        // если оба пользователя отключились, то удаляем комнату из списка
        synchronized (roomList) {
            roomList.removeIf(roomDTO -> (roomDTO.getUserName1() == null && roomDTO.getUserName2() == null));
        }
        // формируем ответ
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(requestDTO.getRequestType());
        responseDTO.setResponseBody(new JSONObject(disconnectRoom));
        return new JSONObject(responseDTO).toString();
    }
    // проверка конца игры (когда все клетки заполнены)
    public boolean checkEndGame(Integer[][] ticTacToe) {
        boolean t = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (ticTacToe[i][j] == 0) {
                    t = false;
                }
            }
        }
        return t;
    }
    // определение как закончилась игра
    public byte whoWinGame (Integer[][] ticTacToe) {
        boolean x;
        boolean o;
        byte answer = 0;
        for (int i = 0; i < 3; i++) {
            x = true;
            o = true;
            for (int j = 0; j < 3; j++) {
                if (ticTacToe[i][j] == 1 || ticTacToe[i][j] == 0) {
                    o = false;
                }
                if (ticTacToe[i][j] == 2 || ticTacToe[i][j] == 0) {
                    x =false;
                }
            }
            if (x) {
                answer = 1;
            }
            if (o) {
                answer = 2;
            }
        }

        for (int i = 0; i < 3; i++) {
            x = true;
            o = true;
            for (int j = 0; j < 3; j++) {
                if (ticTacToe[j][i] == 1 || ticTacToe[j][i] == 0) {
                    o = false;
                }
                if (ticTacToe[j][i] == 2 || ticTacToe[j][i] == 0) {
                    x =false;
                }
            }
            if (x) {
                answer = 1;
            }
            if (o) {
                answer = 2;
            }
        }

        for (int i = 0; i < 2; i++) {
            x = true;
            o = true;
            for (int j = 0; j < 3; j++) {
                if (i != 0) {
                    if (ticTacToe[j][i * 2 - j] == 1 || ticTacToe[j][i * 2 - j] == 0) {
                        o = false;
                    }
                    if (ticTacToe[j][i * 2 - j] == 2 || ticTacToe[j][i * 2 - j] == 0) {
                        x = false;
                    }
                } else {
                    if (ticTacToe[j][j] == 1 || ticTacToe[j][j] == 0) {
                        o = false;
                    }
                    if (ticTacToe[j][j] == 2 || ticTacToe[j][j] == 0) {
                        x = false;
                    }
                }
            }
            if (x) {
                answer = 1;
            }
            if (o) {
                answer = 2;
            }
        }
        return answer;
    }

    // удаляет подключение пользователя из спика
    public boolean closeClientConnection (ServerController serverController) {
        Server.serverList.remove(serverController);
        return false;
    }

    // попытка подключения пользователя
    public String tryUserConnection(ServerController serverController, JSONObject jsonObject) {
        // получаем заголовок и тело входящего запроса
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType(String.valueOf(jsonObject.get("requestType")));
        requestDTO.setRequestBody(jsonObject.getJSONObject("requestBody"));
        // получаем имя пользователя
        // если в какой-либо комнате есть это имя пользователя
        // то добавляет текущее подключение пользователя
        String userName = requestDTO.getRequestBody().getString("userName");
        for (RoomDTO roomDTO: roomList) {
            if (Objects.equals(roomDTO.getUserName1(), userName)) {
                roomDTO.setServerController1(serverController);
            } else {
                if (Objects.equals(roomDTO.getUserName2(), userName)) {
                    roomDTO.setServerController2(serverController);
                }
            }
        }
        // формируем ответ от сервера
        CheckConnectionDTO checkConnectionDTO = new CheckConnectionDTO();
        checkConnectionDTO.setUserName(userName);

        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(requestDTO.getRequestType());
        responseDTO.setResponseBody(new JSONObject(checkConnectionDTO));

        return new JSONObject(responseDTO).toString();
    }

    public JSONObject dataForKafka(String typeRequest, String userName, Integer idRoom, Integer cell, Integer typeCell) {
        // данные, которые отправляются в кафку
        KafkaDTO kafkaDTO = new KafkaDTO();
        kafkaDTO.setIdServer(Integer.valueOf(Configs.property.getProperty("kafka.id.server")));
        kafkaDTO.setTypeRequest(typeRequest);
        kafkaDTO.setUserName(userName);
        kafkaDTO.setIdRoom(idRoom);
        kafkaDTO.setCellNumber(cell);
        kafkaDTO.setTypeCell(typeCell);
        return new JSONObject(kafkaDTO);
    }
}
