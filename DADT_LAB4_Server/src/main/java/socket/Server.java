package socket;

import configs.Configs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
    public static final int PORT = Integer.parseInt(Configs.property.getProperty("socket.port"));
    public static LinkedList<ServerController> serverList = new LinkedList<>(); // список всех нитей

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Сервер запущен! Готов принимать подключения!");
            ExecutorService pool = Executors.newCachedThreadPool();
            try {
                while (true) {
                    Socket socket = serverSocket.accept();
                    pool.execute(() -> {
                        try {
                            new ServerController(socket);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            } finally {
                pool.shutdown();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
