package kafka.producer.kafkaconnectionproducer;

import org.json.JSONObject;
import tictactoe_schema.avro.tictactoe;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Logger;
import kafka.producer.configs.KafkaConnectionConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Future;

public class KafkaConnection {

    static Logger logger = Logger.getLogger(KafkaConnection.class);

    private static KafkaProducer<String,tictactoe> createProducer() {
        return new KafkaProducer<>(KafkaConnectionConfig.CONFIG.getProducerProperties());
    }

    public static void SendData(JSONObject jsonObject) throws SQLException {

        ArrayList<tictactoe> dataForProducingList = new ArrayList<>();

        logger.info("Формирование списка сообщений с avro-серриализацией для отправки в Kafka...");
        Integer idServer = null; String typeRequest = null; String userName = null; Integer idRoom = null; Integer cellNumber = null; Integer typeCell = null;
        try {idServer = (Integer) jsonObject.get("idServer");} catch (Exception e) {System.out.println(e.getMessage());};
        try {typeRequest = jsonObject.get("typeRequest").toString();} catch (Exception e) {System.out.println(e.getMessage());};
        try {userName = jsonObject.get("userName").toString();} catch (Exception e) {System.out.println(e.getMessage());};
        try {idRoom = (Integer) jsonObject.get("idRoom");} catch (Exception e) {System.out.println(e.getMessage());};
        try {cellNumber = (Integer) jsonObject.get("cellNumber");} catch (Exception e) {System.out.println(e.getMessage());};
        try {typeCell = (Integer) jsonObject.get("typeCell");} catch (Exception e) {System.out.println(e.getMessage());}

        dataForProducingList.add(tictactoe.newBuilder()
                .setIdServer(idServer)
                .setTypeRequest(typeRequest)
                .setUsername(userName)
                .setIdRoom(idRoom)
                .setIdCell(cellNumber)
                .setTypeCell(typeCell)
                .build());

        if (!dataForProducingList.isEmpty()) {
            logger.info("Подключение к Kafka...");
            Future<RecordMetadata> result;
            try(KafkaProducer<String, tictactoe> producer = createProducer()) {
                logger.info("Подключение к Kafka успешно установлено. Отправка сообщений...");
                for (tictactoe orderEvent : dataForProducingList) {
                    ProducerRecord<String, tictactoe> record = new ProducerRecord<>(KafkaConnectionConfig.CONFIG.getKafkaTopicName(), orderEvent);
                    result = producer.send(record);
                    RecordMetadata sendResult = result.get();
                    if (!sendResult.hasOffset()) {
                        throw new Exception("Ошибка отправки данных! Offset не получен."); // Признаком успешной записи данных в Kafka является полученный offset в Kafka
                    }
                 }
                dataForProducingList.clear();
                logger.info("Данные отправлены успешно.");
             }
             catch (Exception e) {
                logger.error("Ошибка отправки данных! " + e.getMessage());
            }
        }
        else {
            logger.info("В таблице отсутствуют новые данные.");
        }
    }
}
