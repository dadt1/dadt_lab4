package kafka.producer.configs;

import configs.Configs;
import lombok.Getter;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SaslConfigs;

import java.util.Properties;

public enum KafkaConnectionConfig {
    CONFIG;

    private @Getter String kafkaTopicName;
    private final @Getter Properties  producerProperties = new Properties();
    private @Getter int maxMessagesInIteration;
    private @Getter int loggingPartitionsSize;

    KafkaConnectionConfig() {
        try {
        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Configs.property.getProperty("kafka.bootstrap.servers"));
        producerProperties.setProperty("schema.registry.url", Configs.property.getProperty("kafka.schema.registry"));
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"io.confluent.kafka.serializers.KafkaAvroSerializer");
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"io.confluent.kafka.serializers.KafkaAvroSerializer");

        producerProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_PLAINTEXT");
        producerProperties.put(SaslConfigs.SASL_MECHANISM,"PLAIN");

        producerProperties.setProperty(SaslConfigs.SASL_JAAS_CONFIG,"org.apache.kafka.common.security.scram.ScramLoginModule required " +
                    "username=\""+Configs.property.getProperty("kafka.username")+"\" password=\""+Configs.property.getProperty("kafka.password")+"\";");

        kafkaTopicName = Configs.property.getProperty("kafka.topic.name");

        loggingPartitionsSize = 20;

        maxMessagesInIteration = 100000;
        } catch (Exception e) {
            System.out.printf("Ошибка загрузки конфигурации подключения к Kafka. Текст ошибки: %s%n", e.getMessage());
        }
    }
}
