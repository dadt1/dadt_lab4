package kafka.consumer.configs;

import configs.Configs;
import kafka.consumer.kafkaconnectionproducer.KafkaConnection;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import lombok.Getter;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;

import java.util.Properties;

public enum KafkaConnectionConfig {
    CONFIG;

    private @Getter String kafkaTopicName;
    private @Getter String consumerGroup;
    private @Getter int pollTimeoutSeconds;
    private final @Getter Properties  consumerProperties = new Properties();

    KafkaConnectionConfig() {
        Logger logger = Logger.getLogger(KafkaConnection.class);
        try {

            kafkaTopicName = Configs.property.getProperty("kafka.topic.name");
            consumerGroup = Configs.property.getProperty("kafka.consumer.group");

            //Работа с данными
            consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, Configs.property.getProperty("kafka.bootstrap.servers"));
            consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup);
            consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
            consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,"false");
            consumerProperties.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG,"false");

            //AVRO
            consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
            consumerProperties.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "true");
            consumerProperties.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, Configs.property.getProperty("kafka.schema.registry"));

            // Объём данных в байтах
            consumerProperties.put(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG,Configs.property.getProperty("kafka.max.partition.fetch.bytes.config"));

            //Количество записей
            consumerProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,Configs.property.getProperty("kafka.max.poll.records.config"));

            //Время, необходимое, чтобы считать записи
            pollTimeoutSeconds = Integer.parseInt(Configs.property.getProperty("kafka.max.poll.records.config.seconds"));

            consumerProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_PLAINTEXT");
            consumerProperties.put(SaslConfigs.SASL_MECHANISM,"PLAIN");

            consumerProperties.put(SaslConfigs.SASL_JAAS_CONFIG,"org.apache.kafka.common.security.scram.ScramLoginModule required " +
                    "username=\""+Configs.property.getProperty("kafka.username")+"\" password=\""+Configs.property.getProperty("kafka.password")+"\";");

        } catch (Exception e) {
            logger.error("Ошибка загрузки конфигурации подключения к Kafka. Текст ошибки: " + e.getMessage());
        }
    }
}

