package kafka.consumer;

import kafka.consumer.configs.MainConfig;
import kafka.consumer.timertask.TicTacTimerTask;
import org.apache.log4j.Logger;
import java.util.Timer;
import java.util.TimerTask;

public class Consumer {
    static Logger logger = Logger.getLogger(Consumer.class);
    public static void runConsumer() {

        final int kafkaConsumeIntervalMs = MainConfig.CONFIG.getKafkaConsumeIntervalSeconds() * 1000;// *1000 - перевод из секунд в миллисекунды для метода timer.schedule
        final int kafkaDelayIntervalMs = MainConfig.CONFIG.getKafkaDelayIntervalSeconds() * 1000;

        Timer timer = new Timer();
        TimerTask timerTask = new TicTacTimerTask();
        // Таймер служит для опроса топика Кафки и в случае, если появились новые данные,
        // вычитывает их оттуда и применяет изменения с определённым временным промежутком.
        try {
            timer.schedule(timerTask, kafkaDelayIntervalMs, kafkaConsumeIntervalMs);
        } catch (Exception ex) {
            logger.error(String.format("Ошибка при создании таймера: %s", ex.getMessage()));
        }
    }

}
