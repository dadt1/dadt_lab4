package dto;

import lombok.Data;

@Data
public class CheckConnectionDTO {
    public String userName;
}
