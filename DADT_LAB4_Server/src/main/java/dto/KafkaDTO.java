package dto;

import lombok.Data;
import socket.ServerController;
import socket.ServerService;

@Data
public class KafkaDTO {
    private Integer idServer;
    private String typeRequest;
    private String userName;
    private Integer idRoom;
    private Integer cellNumber;
    private Integer typeCell;
}
