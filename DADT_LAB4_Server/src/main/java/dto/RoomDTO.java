package dto;

import lombok.Data;
import socket.ServerController;

@Data
public class RoomDTO {
    private Integer roomId;
    private String userName1;
    private String userName2;
    private ServerController serverController1;
    private ServerController serverController2;
    private Integer[][] ticTacToeArray;
}
