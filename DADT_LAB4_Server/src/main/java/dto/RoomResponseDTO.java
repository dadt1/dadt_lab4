package dto;

import lombok.Data;

@Data
public class RoomResponseDTO {
    private String roomStatus;
    private Integer idRoom;
}
