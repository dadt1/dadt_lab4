package dto;

import lombok.Data;

@Data
public class CellResponseDTO {
    private String cellStatus;
    private String userName;
    private Integer idRoom;
    private Integer cellNumber;
    private byte typeCell;
    private String gameStatus;
}
