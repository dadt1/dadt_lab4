package dto;

import lombok.Data;

@Data
public class EndGameDTO {
    private String statusGame;
}
