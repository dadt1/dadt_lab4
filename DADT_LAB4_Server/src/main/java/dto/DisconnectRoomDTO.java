package dto;

import lombok.Data;

@Data
public class DisconnectRoomDTO {
    private String disconnectStatus;
    private String userName;
    private Integer idRoom;
}
