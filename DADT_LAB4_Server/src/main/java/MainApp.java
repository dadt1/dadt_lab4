import configs.Configs;
import kafka.consumer.Consumer;
import socket.Server;

public class MainApp {
    public static void main(String[] args) {
        try {
            Configs.property.load(Configs.input);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Consumer.runConsumer();
        Server server = new Server();
        server.run();
    }
}
