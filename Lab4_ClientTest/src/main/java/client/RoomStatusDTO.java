package client;

import lombok.Data;

@Data
public class RoomStatusDTO {
    private byte status;
    private Integer roomNumber;
}
