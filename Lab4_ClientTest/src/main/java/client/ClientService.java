package client;

import dto.*;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.Objects;


public class ClientService {
    private Socket clientSocket;
    private BufferedReader in;
    private BufferedWriter out;

    public ClientService(Socket clientSocket, BufferedReader in, BufferedWriter out) {
        this.clientSocket = clientSocket;
        this.in = in;
        this.out = out;
    }

    public String takeUsername(String username) throws Exception{//запрос серверу о взятии имени
        System.out.println("takeUsername");
        RequestDTO requestDTO = new RequestDTO(); // формируем запрос
        requestDTO.setRequestType("take_username");

        RequestUserNameDTO requestUserNameDTO = new RequestUserNameDTO();
        requestUserNameDTO.setUserName(username);
        requestDTO.setRequestBody(new JSONObject(requestUserNameDTO));

        JSONObject jsonObject = new JSONObject(requestDTO);
        System.out.println(requestDTO);
        out.write(jsonObject + "\n");
        out.flush();
        System.out.println("Отправили сообщение");
        return in.readLine();
    }

    public String createRoom(String username) throws IOException {// запрос серверу на создание комнаты
        RequestDTO request = new RequestDTO(); // формируем запрос
        request.setRequestType("create_room");

        RequestCreateRoomDTO createRoom = new RequestCreateRoomDTO();
        createRoom.setUserName(username);
        request.setRequestBody((new JSONObject(createRoom)));

        JSONObject jsonObject = new JSONObject(request);
        System.out.println(jsonObject);
        out.write(jsonObject + "\n"); // отправляем сообщение на сервер
        out.flush();
        System.out.println("Отправили сообщение");

        return in.readLine();
    }

    public String connectRoom(String username, Integer id) throws IOException { // запрос серверу на подключение к комнате
        RequestDTO request = new RequestDTO();
        request.setRequestType("connect_to_the_room");

        RequestConnectRoomDTO connect = new RequestConnectRoomDTO();
        connect.setUserName(username);
        connect.setIdRoom(id);
        request.setRequestBody(new JSONObject(connect));

        JSONObject jsonObject = new JSONObject(request);

        out.write(jsonObject + "\n"); // отправляем сообщение на сервер
        out.flush();
        System.out.println("Отправили сообщение");

        return in.readLine();
    }

    public String takeCell(String username, Integer cellNumber, Integer roomNumber) throws IOException { // запрос серверу о произведенном ходе
        RequestDTO request = new RequestDTO();
        request.setRequestType("take_the_cell");
        System.out.println("Выберите клетку");

        RequestTakeCellDTO move = new RequestTakeCellDTO();
        move.setUserName(username);
        move.setIdRoom(roomNumber);
        move.setCellNumber(cellNumber);
        request.setRequestBody(new JSONObject(move));

        JSONObject jsonObject = new JSONObject(request);

        out.write(jsonObject + "\n"); // отправляем сообщение на сервер
        out.flush();
        System.out.println("Отправили сообщение");

        return in.readLine();
    }

    public String disconnectRoom(String username, Integer roomNumber) throws IOException { // запрос серверу об отключении
        RequestDTO request = new RequestDTO();
        request.setRequestType("disconnect_from_the_room");

        RequestDisconnectRoomDTO disconnect = new RequestDisconnectRoomDTO();
        disconnect.setUserName(username);
        disconnect.setIdRoom(roomNumber);
        request.setRequestBody(new JSONObject(disconnect));

        JSONObject jsonObject = new JSONObject(request);

        System.out.println("disconnectRoom"+jsonObject);
        out.write(jsonObject + "\n"); // отправляем сообщение на сервер
        out.flush();
        System.out.println("Отправили сообщение");

        return in.readLine();
    }

    public boolean checkUsernameAnswer(String answer) {//провекра на пустой ответ
        JSONObject jsonAnswer = new JSONObject(answer);
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(jsonAnswer.getString("responseType"));
        responseDTO.setResponseBody(jsonAnswer.getJSONObject("responseBody"));
        if (Objects.equals(responseDTO.getResponseBody().getString("userNameStatus"), "failed"))
            return false;
        else
            return true;
    }

    public boolean checkRoomAnswer(String answer) {// проверка успешности создания комнаты
        JSONObject jsonAnswer = new JSONObject(answer);
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(jsonAnswer.getString("responseType"));
        responseDTO.setResponseBody(jsonAnswer.getJSONObject("responseBody"));
        if (Objects.equals(responseDTO.getResponseBody().getString("roomStatus"), "failed"))
            return false;
        else
            return true;
    }

    public boolean checkDisconnectAnswer(String answer) {
        System.out.println(answer);
        JSONObject jsonAnswer = new JSONObject(answer);
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(jsonAnswer.getString("responseType"));
        responseDTO.setResponseBody(jsonAnswer.getJSONObject("responseBody"));
        String disconnectStatus = responseDTO.getResponseType();
        if (Objects.equals(disconnectStatus, "disconnect_from_the_room")) {
            if (Objects.equals(responseDTO.getResponseBody().getString("disconnectStatus"), "failed"))
                return true;
            else
                return false;
        } else {
            return true;
        }
    }

    public byte checkWinLoseDrawGame(String answer) { // проверка окончания игры
        JSONObject jsonAnswer = new JSONObject(answer);
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponseType(jsonAnswer.getString("responseType"));
        responseDTO.setResponseBody(jsonAnswer.getJSONObject("responseBody"));
        String statusGame = responseDTO.getResponseType();
        if (Objects.equals(statusGame, "take_the_cell")) {
            if (Objects.equals(responseDTO.getResponseBody().getString("gameStatus"), "continue")){
                return 1;
            } else {
                if (Objects.equals(responseDTO.getResponseBody().getString("gameStatus"), "win")) {
                    return 2;
                } else {
                    if (Objects.equals(responseDTO.getResponseBody().getString("gameStatus"), "lose")) {
                        return 3;
                    } else {
                        if (Objects.equals(responseDTO.getResponseBody().getString("gameStatus"), "draw")) {
                            return 4;
                        }
                    }
                }
            }
        }
        return -1;
    }

    public boolean closeConnection() throws IOException {
        RequestDTO request = new RequestDTO();
        request.setRequestType("stop");

        JSONObject jsonObject = new JSONObject(request);

        out.write(jsonObject + "\n"); // отправляем сообщение на сервер
        out.flush();

        System.out.println("Клиент был закрыт...");
        clientSocket.close();
        in.close();
        out.close();

        return false;
    }

    public boolean checkNullAnswer(String answer) {
        return answer == null;
    } //провекра на нулевой ответ


    public String tryUserConnection(String username) throws Exception{ //запрос на переподключение
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setRequestType("try_connect");

        CheckConnectionDTO checkConnectionDTO = new CheckConnectionDTO();
        checkConnectionDTO.setUserName(username);
        requestDTO.setRequestBody(new JSONObject(checkConnectionDTO));

        JSONObject jsonObject = new JSONObject(requestDTO);
        System.out.println(requestDTO);
        out.write(jsonObject + "\n");
        out.flush();
        System.out.println("Отправили сообщение");
        return in.readLine();
    }
}
