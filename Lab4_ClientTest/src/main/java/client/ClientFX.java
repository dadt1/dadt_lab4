package client;

import configs.Configs;
import dto.ResponseDTO;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientFX {
    Socket clientSocket;
    BufferedReader reader;
    BufferedReader in;
    BufferedWriter out;
    ClientService clientService;

    public ClientFX() throws IOException {
        clientSocket = new Socket(Configs.property.getProperty("socket.ip"), Integer.parseInt(Configs.property.getProperty("socket.port")));
        reader = new BufferedReader(new InputStreamReader(System.in));
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        clientService = new ClientService(clientSocket, in, out);
    }

    public byte takeUsername(String userName) throws Exception {//обработка запроса на получение имени
        String answer;
        System.out.println("Введите имя пользователя");
        answer = clientService.takeUsername(userName);
        System.out.println(answer);

        if (clientService.checkNullAnswer(answer)) {
            return 0;
        } else {
            if (clientService.checkUsernameAnswer(answer))
                return 1;
            else
                return 2;
        }
    }

    public RoomStatusDTO createRoom(String userName) throws IOException {//обработка запроса на создание комнаты
        RoomStatusDTO result = new RoomStatusDTO();

        String answer;
        answer = clientService.createRoom(userName);
        System.out.println(answer);

        if (clientService.checkNullAnswer(answer)) {
            result.setStatus((byte) 0);
            return result;
        } else {
            if (clientService.checkRoomAnswer(answer)) {//если комната успешно создана - получаем номер комнаты
                JSONObject jsonObject= new JSONObject(answer);
                ResponseDTO response = new ResponseDTO();
                response.setResponseType(String.valueOf(jsonObject.get("responseType")));
                response.setResponseBody(jsonObject.getJSONObject("responseBody"));
                Integer idRoom = response.getResponseBody().getInt("idRoom");
                result.setRoomNumber(idRoom);
                result.setStatus((byte) 1);
                return result;
            } else {
                result.setStatus((byte) 2);
                return result;
            }
        }
    }

    public byte isRoomConnection(String userName, Integer id) throws IOException {//обработка запроса на подключение к комнате
        String answer;
        answer = clientService.connectRoom(userName, id);

        if (clientService.checkNullAnswer(answer)) {
            return 0;
        } else {
            if (clientService.checkRoomAnswer(answer))
                return 1;
            else
                return 2;
        }
    }

    public byte takeTheCell(String userName, Integer cellNumber, Integer roomNumber) throws IOException {//обрабокта запроса с ходом игрока
        String answer;
        answer = clientService.takeCell(userName, cellNumber, roomNumber);
        System.out.println("takeTheCell");
        System.out.println(answer);

        if (clientService.checkNullAnswer(answer)) {
            return 0;
        } else {
            if (clientService.checkDisconnectAnswer(answer)) {
                JSONObject jsonObject= new JSONObject(answer);
                ResponseDTO response = new ResponseDTO();
                response.setResponseType(String.valueOf(jsonObject.get("responseType")));
                response.setResponseBody(jsonObject.getJSONObject("responseBody"));
                byte cell = (byte) response.getResponseBody().getInt("cellNumber");
                if (clientService.checkWinLoseDrawGame(answer)==1) {//если игра продолжается
                    return (byte) (10+cell);
                } else {
                    if (clientService.checkWinLoseDrawGame(answer) == 2) {//победа
                        return (byte) (20+cell);
                    } else {
                        if (clientService.checkWinLoseDrawGame(answer) == 3) {//поражение
                            return (byte) (30+cell);
                        } else {
                            if (clientService.checkWinLoseDrawGame(answer) == 4) {//ничья
                                return (byte) (40+cell);
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    public byte disconnectFromRoom(String userName, Integer roomNumber) throws IOException {//обработки запроса на отключение от комнаты
        String answer;
        answer = clientService.disconnectRoom(userName, roomNumber);
        if (clientService.checkNullAnswer(answer)) {
            return 0;
        } else {
            return 1;
        }
    }

    public byte closeConnection() throws IOException {//обработки запроса на отключение от комнаты
        if (clientService.closeConnection()) {
            return 0;
        } else {
            return 1;
        }
    }

    public boolean reconnectToServer(String username) throws Exception {//обарбокта запроса на переподключение
        String answer;
        answer = clientService.tryUserConnection(username);
        System.out.println(answer);
        if (answer == null) {
            return false;
        } else {
            return true;
        }
    }

    public byte waitForResponse () throws IOException {//обработка ответа сервера
        String answer;
        answer = in.readLine();
        if (clientService.checkNullAnswer(answer)) {
            return 0;
        } else {
            if (clientService.checkWinLoseDrawGame(answer)==1) {
                JSONObject jsonObject= new JSONObject(answer);
                ResponseDTO response = new ResponseDTO();
                response.setResponseType(String.valueOf(jsonObject.get("responseType")));
                response.setResponseBody(jsonObject.getJSONObject("responseBody"));
                byte cell = (byte) response.getResponseBody().getInt("cellNumber");
                return (byte) (4+cell);
            } else {
                if (clientService.checkWinLoseDrawGame(answer) == 2) {//победа
                    return 1;
                } else {
                    if (clientService.checkWinLoseDrawGame(answer) == 3) {//поражение
                        return 2;
                    } else {
                        if (clientService.checkWinLoseDrawGame(answer) == 4) {//ничья
                            return 3;
                        }
                    }
                }
            }
        }
        return -1;
    }
}
