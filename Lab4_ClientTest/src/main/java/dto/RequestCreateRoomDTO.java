package dto;

import lombok.Data;

@Data
public class RequestCreateRoomDTO {
    private String userName;
}
