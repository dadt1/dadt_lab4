package dto;

import lombok.Data;
import org.json.JSONObject;

@Data
public class ResponseDTO {
    private String responseType;
    private JSONObject responseBody;
}
