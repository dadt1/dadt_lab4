package dto;

import lombok.Data;

@Data
public class RequestTakeCellDTO {
    private String userName;
    private Integer idRoom;
    private Integer cellNumber;
    //private String gameStatus;
}
