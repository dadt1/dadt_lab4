package dto;

import lombok.Data;

@Data
public class RequestUserNameDTO {
    private String userName;
}
