package dto;

import lombok.Data;

@Data
public class RequestDisconnectRoomDTO {
    private String userName;
    private Integer idRoom;
}
