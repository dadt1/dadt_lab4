package dto;

import lombok.Data;
import org.json.JSONObject;

@Data
public class RequestDTO {
    private String requestType;
    private JSONObject requestBody;
}
