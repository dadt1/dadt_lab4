package dto;

import lombok.Data;

@Data
public class RequestConnectRoomDTO {
    private String userName;
    private Integer idRoom;
}
