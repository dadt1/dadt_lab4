package tictactoe.form;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class RoomController {


    private String userName;
    private Integer roomNumber;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    @FXML
    private GridPane gameField;

    @FXML
    private Label roomName;

    Button[] buttons = new Button[9];

    void freeButtonsSetDisable(boolean choice){ // блокировка/разблокировка "свободных" клеток поля
        for (Integer i=0; i<=8; i++) {
            if (Objects.equals(buttons[i].getText(), "-"))
                buttons[i].setDisable(choice);
        }
    }


    void buttonClick(String userName, Integer i, boolean roomOwner) throws Exception{
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("button is clicked");

                Platform.runLater(() -> { // обращаемся к элементам интерфейса
                    if (roomOwner) {
                        buttons[i].setText("X");
                        buttons[i].setDisable(true);
                    } else {
                        buttons[i].setText("0");
                        buttons[i].setDisable(true);
                    }
                    freeButtonsSetDisable(true); // блокируем клетки
                });

                byte byteAnswer = 0;
                try {
                    byteAnswer = TicTacApplication.getClient().takeTheCell(userName, i, roomNumber); //отправляем запрос серверу
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("byteAnswer");
                System.out.println(byteAnswer);

                if (byteAnswer == 0) { // нет подключения
                    Platform.runLater(() -> { // обращаемся к элементам интерфейса

                        /*freeButtonsSetDisable(false); // разблокируем кнопки
                        buttons[i].setText("-");
                        buttons[i].setDisable(false);*/
                        buttons[i].setText("-");
                        buttons[i].setDisable(false);


                        Reconnect reconnectWindow = new Reconnect();
                        reconnectWindow.startWindow(userName);;
                    });
                } else {
                    // продолжаем играть
                    int statusGameNumber = byteAnswer / 10;
                    int buttonNumber = byteAnswer % 10;

                    if (statusGameNumber == 1) {
                        Platform.runLater(() -> { // обращаемся к элементам интерфейса
                            freeButtonsSetDisable(false);
                            if (roomOwner) {
                                buttons[buttonNumber].setText("0");
                            } else {
                                buttons[buttonNumber].setText("X");
                            }
                            buttons[buttonNumber].setDisable(true);

                        });
                    } else {
                        if (statusGameNumber == 2 || statusGameNumber == 3 || statusGameNumber == 4) {
                            ResultGame result = new ResultGame();
                            result.startWindow(statusGameNumber, userName, roomNumber, (Stage) gameField.getScene().getWindow());
                        }
                    }
                }

            }
        }).start();
    }
    @FXML
    void initialize(String username, boolean roomOwner, Integer roomNumber) throws IOException { //инициализируем окно комнаты
        roomName.setText("Комната: "+roomNumber.toString());
        this.roomNumber=roomNumber;
        this.userName=username;

        for (Integer i=0; i<=8; i++) { // создаем кнопки-клетки
            buttons[i] = new Button();
            buttons[i].setText("-");
            buttons[i].setMaxWidth(Double.MAX_VALUE);
            buttons[i].setMaxHeight(Double.MAX_VALUE);
            Integer finalI = i;
            buttons[i].setOnAction(actionEvent -> {
                try {
                    buttonClick(username,finalI,roomOwner);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }

        gameField.add(buttons[0], 0, 0);// добавляем кнопки на игровое поле, i - столбец, i1 - строчка
        gameField.add(buttons[1], 1, 0);
        gameField.add(buttons[2], 2, 0);
        gameField.add(buttons[3], 0, 1);
        gameField.add(buttons[4], 1, 1);
        gameField.add(buttons[5], 2, 1);
        gameField.add(buttons[6], 0, 2);
        gameField.add(buttons[7], 1, 2);
        gameField.add(buttons[8], 2, 2);

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (!roomOwner) { //если не владелец комнаты, то блокируем все клетки и ожидаем ответ сервера
                    Platform.runLater(() -> { // обращаемся к элементам интерфейса
                        for (Integer i = 0; i <= 8; i++) {
                            buttons[i].setDisable(true);
                        }
                    });

                    byte byteAnswer = 0;
                    do {
                        try {
                            byteAnswer = TicTacApplication.getClient().waitForResponse(); //ожидаем ответ сервера
                            System.out.println(byteAnswer);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        if (byteAnswer == 0) { // нет подключения
                            Platform.runLater(() -> { // обращаемся к элементам интерфейса
                                Reconnect reconnectWindow = new Reconnect();
                                reconnectWindow.startWindow(userName);
                            });
                            while (!ReconnectController.roomOwnerConnectRoom) {}
                        }
                    } while (byteAnswer == 0);
                    if (byteAnswer >= 4) { // соперник сходил
                        Integer buttonNumber = byteAnswer - 4;
                        Platform.runLater(() -> { // обращаемся к элементам интерфейса
                            freeButtonsSetDisable(false);  // разблокируем "свободные" клекти
                            if (roomOwner) { //отмечаем ход соперника
                                buttons[buttonNumber].setText("0");
                            } else {
                                buttons[buttonNumber].setText("X");
                            }
                            buttons[buttonNumber].setDisable(true);
                        });
                    }
                }
            }
            }).start();
    }
}