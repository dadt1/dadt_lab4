package tictactoe.form;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class ResultGame {
    public void startWindow(Integer byteAnswer , String userName, Integer roomNumber, Stage parentStage){
        System.out.println("Game over");
        Platform.runLater(() -> { // обращаемся к элементам интерфейса
            FXMLLoader loader = new FXMLLoader(); // открываем окно результата игры
            loader.setLocation(getClass().getResource("result.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Крестики-нолики  Игрок: "+userName +"  Комната: "+roomNumber.toString());

            ResultController resultController = loader.getController();
            resultController.initialize(userName, byteAnswer, roomNumber);
            stage.showAndWait();

            parentStage.close(); //закрываем окно комнаты
        });
    }
}

