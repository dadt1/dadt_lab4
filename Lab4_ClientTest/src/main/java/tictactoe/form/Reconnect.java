package tictactoe.form;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Reconnect {

    public void startWindow(String userName){

        FXMLLoader loader = new FXMLLoader(); // открываем окно переподключения
        loader.setLocation(getClass().getResource("reconnect.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        ReconnectController reconnectController = loader.getController();
        reconnectController.initialize(userName);
        stage.showAndWait();
    }
}
