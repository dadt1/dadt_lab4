package tictactoe.form;
import client.ClientFX;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ReconnectController {

    @FXML
    private Button ReconnectButton;

    private String username;

    // заглушка для подключения пользователя к комнате, в случе обрыва связи
    public static boolean roomOwnerConnectRoom = false;

    @FXML
    void onReconnectButtonClick() throws Exception { // обработчик нажатия кнопки переподключения
        String userName = username;
        boolean connect = true;
        while (connect) {
            try {
                TicTacApplication.setClient(new ClientFX());
                connect = false;
            } catch (Exception e) {
                connect = true;
            }
        }
        if (TicTacApplication.getClient().reconnectToServer(userName)) {
            roomOwnerConnectRoom = true;
            Stage stage = (Stage) ReconnectButton.getScene().getWindow();
            stage.close();
        }
    }

    @FXML
    void initialize(String username) {
        roomOwnerConnectRoom = false;
        this.username = username;
    }

}
