package tictactoe.form;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class ResultController {

    private static String userName;
    private static Integer roomNumber;
    @FXML
    private Button disconnectButton;

    @FXML
    private Label gameResult;

    @FXML
    void onDisconnectButtonClick(ActionEvent event) { //обработчик нажатия кнопки отключения
        new Thread(new Runnable() {
            @Override
            public void run() {

                byte byteAnswer = 0;
                try {
                    byteAnswer = TicTacApplication.getClient().disconnectFromRoom(userName, roomNumber);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                if (byteAnswer == 0) {

                    Platform.runLater(() -> { // обращаемся к элементам интерфейса
                        Reconnect reconnectWindow = new Reconnect();
                        reconnectWindow.startWindow(userName);
                    });

                } else {
                    if (byteAnswer == 1) {//успешно отключились

                        Platform.runLater(() -> { //обращаемся к элементам интерфейса
                            Stage stage = (Stage) disconnectButton.getScene().getWindow();
                            stage.close();
                        });
                    }
                }
            }
        }).start();

    }
    @FXML
    void initialize(String username, Integer outcome, Integer roomnumber){ //инициализация окна результата
        this.userName = username;
        this.roomNumber = roomnumber;

        gameResult.setFont(  new Font(18 ));
        if (outcome == 2) {
            gameResult.setText("Вы выиграли!");
        }else{
            if (outcome == 3){
                gameResult.setText("Вы проиграли!");

            } else {
                if (outcome == 4)
                gameResult.setText("Победила дружба!");
            }
        }

    }

}
