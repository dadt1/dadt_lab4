package tictactoe.form;

import client.RoomStatusDTO;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartController {

    public static String userName;
    private static Integer roomNumber;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    @FXML
    private Label welcomeText;

    @FXML
    private Button connectRoomButton;

    @FXML
    private Button createRoomButton;

    @FXML
    private TextField roomNumberTextBox;


    @FXML
    private Button loginButton;

    @FXML
    private Button quitTheGameButton;

    @FXML
    private TextField userNameTextBox;

    @FXML
    private Label messageLabel;

    void disconnect() throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {

                byte byteAnswer = 0;
                try {
                    byteAnswer = TicTacApplication.getClient().disconnectFromRoom(userName, roomNumber); //запрос на отключение от комнаты
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                if (byteAnswer == 1) {//успешно отключились

                    /*Platform.runLater(() -> {
                    });*/
                }
            }


        }).start();
    }
    @FXML
    void onLoginButtonClick() throws Exception { // обработчик нажайтия кнопки Войти
        String userName = userNameTextBox.getText();
        this.userName = userName;

        byte byteAnswer = TicTacApplication.getClient().takeUsername(userName);
        if (byteAnswer == 0) { // нет подключения
            Reconnect reconnectWindow = new Reconnect();
            reconnectWindow.startWindow(userName);
        } else {
            // имя успешно взяли
            if (byteAnswer == 1) {
                connectRoomButton.setDisable(false);
                createRoomButton.setDisable(false);
                loginButton.setDisable(true);
                userNameTextBox.setDisable(true);
                messageLabel.setText("");
            } else {
                // такое имя уже есть
                if (byteAnswer == 2) {
                    connectRoomButton.setDisable(true);
                    createRoomButton.setDisable(true);
                    loginButton.setDisable(false);
                    userNameTextBox.setDisable(false);
                    messageLabel.setText("Данное имя уже занято");
                }
            }
        }
    }

    @FXML
    void onCreateRoomButtonClick() throws Exception { // обработчик нажайтия кнопки Создать комнату
        String userName = userNameTextBox.getText();
        this.userName = userName;

        createRoomButton.setDisable(true);
        connectRoomButton.setDisable(true);
        roomNumberTextBox.setDisable(true);
        RoomStatusDTO roomStatus = new RoomStatusDTO();

        roomStatus = TicTacApplication.getClient().createRoom(userName);
        byte byteAnswer = roomStatus.getStatus();
        System.out.println("чзх "  + roomStatus);
        if (byteAnswer == 0) { // нет подключения
            Reconnect reconnectWindow = new Reconnect();
            reconnectWindow.startWindow(userName);
            createRoomButton.setDisable(false);
            connectRoomButton.setDisable(false);
            roomNumberTextBox.setDisable(false);
        } else {
            // имя успешно взяли
            if (byteAnswer == 1) {
                messageLabel.setText("");
                this.roomNumber= roomStatus.getRoomNumber();

                FXMLLoader loader = new FXMLLoader(); // открываем окно комнаты
                loader.setLocation(getClass().getResource("room.fxml"));
                try {
                    loader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                Parent root = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Крестики-нолики  Игрок: "+userName +"  Комната: "+roomStatus.getRoomNumber());

                stage.setOnCloseRequest(new EventHandler<WindowEvent>() { //действия на закрытие окна
                    @Override
                    public void handle(WindowEvent event) {
                        System.out.println("Close");

                        createRoomButton.setDisable(false);
                        connectRoomButton.setDisable(false);
                        roomNumberTextBox.setDisable(false);
                        try {
                            disconnect();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                    }
                });

                RoomController controller = loader.getController();
                controller.initialize(userName,true, roomStatus.getRoomNumber());
                stage.showAndWait();
                createRoomButton.setDisable(false);
                connectRoomButton.setDisable(false);
                roomNumberTextBox.setDisable(false);

            } else {
                // такое имя уже есть
                if (byteAnswer == 2) {
                    // ничего
                }
            }
        }
    }
    public void onConnectRoomButtonClick() throws Exception {

        createRoomButton.setDisable(true);
        connectRoomButton.setDisable(true);
        roomNumberTextBox.setDisable(true);


        String userName = userNameTextBox.getText();
        Integer id = Integer.valueOf(roomNumberTextBox.getText());
        byte byteAnswer = TicTacApplication.getClient().isRoomConnection(userName, id);

        if (byteAnswer == 0) {
            Reconnect reconnectWindow = new Reconnect();
            reconnectWindow.startWindow(userName);
            createRoomButton.setDisable(false);
            connectRoomButton.setDisable(false);
            roomNumberTextBox.setDisable(false);
        } else {
            if (byteAnswer == 1){
                messageLabel.setText("");
                this.roomNumber= id;
                FXMLLoader loader = new FXMLLoader(); // открываем окно комнаты
                loader.setLocation(getClass().getResource("room.fxml"));
                try {
                    loader.load();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                Parent root = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Крестики-нолики  Игрок: "+userName +"  Комната: "+id.toString());



                stage.setOnCloseRequest(new EventHandler<WindowEvent>() { // дейсвтия на закрытие окна
                    @Override
                    public void handle(WindowEvent event) {
                        System.out.println("Close");

                        createRoomButton.setDisable(false);
                        connectRoomButton.setDisable(false);
                        roomNumberTextBox.setDisable(false);

                        try {
                            disconnect();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                    }
                });

                RoomController controller = loader.getController();
                controller.initialize(userName,false, id); // инициализация окна
                stage.showAndWait();
                createRoomButton.setDisable(false);
                connectRoomButton.setDisable(false);
                roomNumberTextBox.setDisable(false);
            } else{
                if (byteAnswer == 2) {
                    createRoomButton.setDisable(false);
                    connectRoomButton.setDisable(false);
                    roomNumberTextBox.setDisable(false);
                    messageLabel.setText("Некорректная комната");
                }
            }

        }
    }

    @FXML
    void onQuitTheGameButtonClick() throws Exception { // обработчик кнопки Выйти из игры
        try {
            TicTacApplication.getClient().closeConnection(); //отключение от сервера
            Stage stage = (Stage) quitTheGameButton.getScene().getWindow();
            stage.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void initialize() {
        connectRoomButton.setDisable(true);
        createRoomButton.setDisable(true);
    }

}