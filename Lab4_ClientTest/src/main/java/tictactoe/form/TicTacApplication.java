package tictactoe.form;

import client.ClientFX;
import configs.Configs;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class TicTacApplication extends Application {

    public static ClientFX client;
    public static ClientFX getClient(){
        return client;
    }
    public static void setClient(ClientFX clientFX){
        client = clientFX;
    }


    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(TicTacApplication.class.getResource("start.fxml")); // запускаем стартовое окно
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Крестики-нолики");
        stage.setScene(scene);
        stage.setResizable(false);
        try {
            Configs.property.load(Configs.input);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() { //дейсвтия по закрытию окна
            @Override
            public void handle(WindowEvent event) {
                try {
                    TicTacApplication.getClient().closeConnection();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        boolean connect = true;
        while (connect) {
            try {
                TicTacApplication.setClient(new ClientFX());
                connect = false;
            } catch (Exception e) {
                connect = true;
            }
        }
        StartController controller = fxmlLoader.getController();
        controller.initialize();
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}